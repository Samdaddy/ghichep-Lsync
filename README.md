Giải pháp đồng bộ dữ liệu , sử dụng Lsyncd 
Rsync nhẹ, cho tốc độ nhanh, nhưng có một điểm yếu là chưa tích hợp tính năng Live, tức là mọi thay đổi chưa được đồng bộ ngay lập tức. Rsync cũng chưa cấu hình được để đồng bộ 2 chiều trong trường hợp Load Balancing. Và để thực hiện tự động thì cần đặt lịch (tạo cron) cho Rsync.

Để giải quyết các tình trạng mà Rsync không làm được, chúng ta có thể sử dụng Lsyncd

Lsyncd (Live Syncing Daemon), sử dụng tiện ích rsync, kết hợp với SSH. Lsyncd nhẹ, cài đặt và cấu hình đơn giản. Tôi sẽ sử dụng Lsyncd để đồng bộ dữ liệu giữa 2 servers CentOS

Chú ý: Thực hiện cài đặt SSH key 2 chiều cho  Server01 và Server02

1. Cài đặt Rsync và  Lsyncd cho cả 2 server

Yêu cầu:  Tham khảo hướng dẫn cài đặt SSH giữa server

sudo yum -y install rsync
sudo yum -y install lsyncd
Tạo thư mục và file chứa cấu hình, log:

mkdir -p /var/log/lsyncd
touch /var/log/lsyncd/lsyncd.{log,status}
2. Cấu hình Lsyncd

Chỉnh sửa file sudo vi  /etc/lsyncd.conf

a. Server01

Nội dung như sau:

----
-- User configuration file for lsyncd.
--
-- Simple example for default rsync, but executing moves through on the target.
--
-- For more examples, see /usr/share/doc/lsyncd*/examples/
--
settings {
 logfile = "/var/log/lsyncd.log", -- Sets the log file
 statusFile = "/var/log/lsyncd-status.log" -- Sets the status log file
 }
 sync{
 default.rsyncssh, -- uses the rsyncssh defaults Take a look here: https://github.com/axkibe/lsyncd/blob/master/default-rsyncssh.lua
 delete = true, -- Doesn't delete files on the remote host eventho they're deleted at the source. This might be beneficial for some not for others
 source="/home/test.vn/", -- Your source directory to watch
 host="192.168.1.202", -- The remote host (use hostname or IP)
 exclude={"/storage"}, -- To exclude file & folder (not sync)
 targetdir="/home/test.vn/", -- the target dir on remote host, keep in mind this is absolute path
 rsync = {
 archive = true, -- use the archive flag in rsync
 perms = true, -- Keep the permissions
 owner = true, -- Keep the owner
 _extra = {"-a"}, -- Sometimes permissions and owners isn't copied correctly so the _extra can be used for any flag in rsync
 },
 delay = 5, -- We want to delay the syncing for 5 seconds so we queue up the events
 maxProcesses = 1, -- We only want to use a maximum of 4 rsync processes at same time
 ssh = {
 port =22 -- This section allows configuration for ssh specific stuff such as a different port
 }
 }
 

b. Server02

Nội dung như sau:

----
-- User configuration file for lsyncd.
--
-- Simple example for default rsync, but executing moves through on the target.
--
-- For more examples, see /usr/share/doc/lsyncd*/examples/
--
settings {
 logfile = "/var/log/lsyncd.log", -- Sets the log file
 statusFile = "/var/log/lsyncd-status.log" -- Sets the status log file
 }
 sync{
 default.rsyncssh, -- uses the rsyncssh defaults Take a look here: https://github.com/axkibe/lsyncd/blob/master/default-rsyncssh.lua
 delete = true, -- Doesn't delete files on the remote host eventho they're deleted at the source. This might be beneficial for some not for others
 source="/home/test.vn/", -- Your source directory to watch
 host="192.168.1.201", -- The remote host (use hostname or IP)
 exclude={"/storage"}, -- To exclude file & folder (not sync)
 targetdir="/home/test.vn/", -- the target dir on remote host, keep in mind this is absolute path
 rsync = {
 archive = true, -- use the archive flag in rsync
 perms = true, -- Keep the permissions
 owner = true, -- Keep the owner
 _extra = {"-a"}, -- Sometimes permissions and owners isn't copied correctly so the _extra can be used for any flag in rsync
 },
 delay = 5, -- We want to delay the syncing for 5 seconds so we queue up the events
 maxProcesses = 1, -- We only want to use a maximum of 4 rsync processes at same time
 ssh = {
 port =22 -- This section allows configuration for ssh specific stuff such as a different port
 }
 }
c. Khởi động dịch vụ và check log

systemctl start lsyncd
tail -f /var/log/lsyncd.log
Như vậy khi 1 trong 2 server có sự thay đổi cập nhật dữ liệu sẽ tự động được đồng bộ sang server còn lại